/****************************************************************************
** Meta object code from reading C++ file 'cryptofile.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../cryptofile.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cryptofile.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_stringModel_t {
    QByteArrayData data[1];
    char stringdata0[12];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_stringModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_stringModel_t qt_meta_stringdata_stringModel = {
    {
QT_MOC_LITERAL(0, 0, 11) // "stringModel"

    },
    "stringModel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_stringModel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void stringModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject stringModel::staticMetaObject = {
    { &QAbstractListModel::staticMetaObject, qt_meta_stringdata_stringModel.data,
      qt_meta_data_stringModel,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *stringModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *stringModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_stringModel.stringdata0))
        return static_cast<void*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int stringModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_cryptofile_t {
    QByteArrayData data[10];
    char stringdata0[67];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_cryptofile_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_cryptofile_t qt_meta_stringdata_cryptofile = {
    {
QT_MOC_LITERAL(0, 0, 10), // "cryptofile"
QT_MOC_LITERAL(1, 11, 3), // "enc"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 4), // "mesg"
QT_MOC_LITERAL(4, 21, 11), // "encryptFile"
QT_MOC_LITERAL(5, 33, 7), // "message"
QT_MOC_LITERAL(6, 41, 5), // "char*"
QT_MOC_LITERAL(7, 47, 3), // "key"
QT_MOC_LITERAL(8, 51, 2), // "iv"
QT_MOC_LITERAL(9, 54, 12) // "DencryptFile"

    },
    "cryptofile\0enc\0\0mesg\0encryptFile\0"
    "message\0char*\0key\0iv\0DencryptFile"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_cryptofile[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   29,    2, 0x0a /* Public */,
       4,    3,   32,    2, 0x0a /* Public */,
       9,    2,   39,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QByteArray, 0x80000000 | 6, 0x80000000 | 6,    5,    7,    8,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6,    7,    8,

       0        // eod
};

void cryptofile::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        cryptofile *_t = static_cast<cryptofile *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->enc((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->encryptFile((*reinterpret_cast< QByteArray(*)>(_a[1])),(*reinterpret_cast< char*(*)>(_a[2])),(*reinterpret_cast< char*(*)>(_a[3]))); break;
        case 2: _t->DencryptFile((*reinterpret_cast< char*(*)>(_a[1])),(*reinterpret_cast< char*(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject cryptofile::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_cryptofile.data,
      qt_meta_data_cryptofile,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *cryptofile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *cryptofile::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_cryptofile.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int cryptofile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
