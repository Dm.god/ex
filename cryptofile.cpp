#include "cryptofile.h"
#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // сами криптогрфические функции https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка
#include <openssl/aes.h>
#include <QDebug>




stringOb::stringOb(const QString &login, const QString &pass) // конструктор
    :   ob_login(login),
        ob_pass(pass)
{

}


QString stringOb::login() const { // возвращает id
    return ob_login;
}

QString stringOb::pass() const { // возвращает имя
    return ob_pass;
}





stringModel::stringModel(QObject *parent)
{

}

void stringModel::addString(const stringOb &newString) // Добавление друга в список
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount()); // сигнал для qml, что начинается запись друга в список
    ob_strings << newString; // запись в список
    endInsertRows(); // сигнал в qml, что пора обновить отображение, т.к. запись окончена
}


int stringModel::rowCount(const QModelIndex &parent) const // возвращает размер списка друзей
{
    Q_UNUSED(parent);
    return ob_strings.count();

}

QVariant stringModel::data(const QModelIndex &index, int role) const // эта функция для возвращения значений переменных по вызову их ролей
{
    if (index.row() < 0 || index.row() >= ob_strings.count())
            return QVariant();
    const stringOb &itemToReturn = ob_strings[index.row()];
    if (role == loginRole) // если вызвана роль Id друга, то возвращается Id друга и т.д.
        return itemToReturn.login();
    else if (role == passRole)
        return itemToReturn.pass();

    return QVariant();


}

QHash<int, QByteArray> stringModel::roleNames() const // соотнесение ролей и их имен, как они будут вызываться из qml
{
    QHash<int, QByteArray> roles;
    roles[loginRole] = "login";
    roles[passRole] = "pass";
    return roles;
}





cryptofile::cryptofile(QObject *parent) : QObject(parent)
{

}

void cryptofile::enc(QString mesg)
{
 QByteArray m;
 m.append(mesg);
 qDebug() << m;
 encryptFile(m,"1234567890", "987654321");
}

void cryptofile::encryptFile(QByteArray message, char *key, char *iv)
{


    QString out_file_name;
#ifdef Q_OS_Android
    // для андроида заданы не каталоги по умолчанию, а просто пути на SD-карте
    // так как папка программы по умолчанию не позволяет писать туда файлы согласно политике безопасности андроид
    out_file_name = "/storage/emulated/0/Download/f_encrypted.txt";
    in_file_name = "/storage/emulated/0/Download/f0.txt";
#else
    out_file_name = "f_encrypted.txt";
#endif

    QBuffer f0(&message);
    f0.open(QIODevice::ReadWrite);
    QFile f_encrypted("f_encrypted.txt");
    f_encrypted.open(QIODevice::WriteOnly | QIODevice::Truncate);//файл для зашифрованых данных
    f0.seek(0);
    //qDebug() << f0.readAll();
    char buffer[256] = {0};
    char out_buf[256] = {0};
    EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();
    EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(),NULL,(unsigned char *)key,(unsigned char *)iv);
    //cbc независимое шифрование порций
    //iv вектор с рандомными числами для шифрования
    int len1 = 0;
    // Qfile read не только считывает данные но и двигает текущую позицию
    int len2 = f0.read(buffer, 256);
    while(len2 > 0)
    {
        EVP_EncryptUpdate(ctx, //обьект с настройками
                          (unsigned char *)out_buf, // входной параметр: ссылка, куда помещать
                          &len1, (unsigned char *)buffer, // выходной параметр: длина полученного шифра
                          len2);
        qDebug()<< len1;// входной параметр: длина входных данных
        f_encrypted.write(out_buf, len1); // вывод зашифрованной порции в файл
        qDebug()<< len2;
        len2 = f0.read(buffer, 256); // считываение следующей порции
    }
    EVP_EncryptFinal_ex(ctx, (unsigned char *)out_buf, &len1);
    f_encrypted.write(out_buf, len1);
    f_encrypted.close();
    f0.seek(0);
    qDebug() << f0.readAll();
    f0.close();
}

void cryptofile::DencryptFile(char *key, char *iv)
{
        QString out_file_name;
    #ifdef Q_OS_Android
        // для андроида заданы не каталоги по умолчанию, а просто пути на SD-карте
        // так как папка программы по умолчанию не позволяет писать туда файлы согласно политике безопасности андроид
        out_file_name = "/storage/emulated/0/Download/f_encrypted.txt";
        in_file_name = "/storage/emulated/0/Download/f0.txt";
    #else
        out_file_name = "f_encrypted.txt";
    #endif


        QFile f_encrypted("f_encrypted.txt");
        QBuffer f1;

        f_encrypted.open(QIODevice::ReadOnly);//файл с исходными данными
        f1.open(QIODevice::ReadWrite | QIODevice::Truncate);//файл для зашифрованых данных
        f1.seek(0);
        char buffer[256] = {0};
        char out_buf[256] = {0};
        EVP_CIPHER_CTX * ctx = EVP_CIPHER_CTX_new();
        EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(),NULL,(unsigned char *)key,(unsigned char *)iv);
        //cbc независимое шифрование порций
        //iv вектор с рандомными числами для шифрования
        int len1 = 0;
        int len2 = f_encrypted.read(buffer, 256);
        while(len2 > 0)
        {
            qDebug()<<"*** before EVP_DecryptUpdate";// входной параметр: длина входных данных

            EVP_DecryptUpdate(ctx, //обьект с настройками
                              (unsigned char *)out_buf, // входной параметр: ссылка, куда помещать
                              &len1, (unsigned char *)buffer, // выходной параметр: длина полученного шифра
                              len2);
            f1.write(out_buf, len1); // вывод зашифрованной порции в файл
            qDebug() << f1.write(out_buf, len1);
            len2 = f_encrypted.read(buffer, 256); // считываение следующей порции
        }
        EVP_DecryptFinal_ex(ctx, (unsigned char *)out_buf, &len1);
        f1.write(out_buf, len1);
        f1.seek(0);
        msg = f1.readAll();
        qDebug() << msg;
        f1.close();
        f_encrypted.close();
        int pos1;
        QString log, pas;
        pos1 = msg.indexOf(" ");
        log = msg.mid(0, pos1);
        pas = msg.mid(pos1 + 1, msg.length() - pos1);
        qDebug() << log << pas;
        mod.addString(stringOb(log, pas));

}
