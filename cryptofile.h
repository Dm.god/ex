#ifndef CRYPTOFILE_H
#define CRYPTOFILE_H

#include <QObject>
#include <QFile>
#include <QByteArray>
#include <QBuffer>
#include <QAbstractListModel>

class stringOb { // описание объекта друга вк
    public:
        stringOb (const QString &login, //сам объект, который имеет 4 параметра id, имя, ссылку на фото и статус
                       const QString &pass);

        QString login() const; // создание функций для возврата переменных параметров соответственно
        QString pass() const;
    private:
        QString ob_login;   // создание самих переменных параметров, которые будут возвращаться
        QString ob_pass;
};




class stringModel : public QAbstractListModel { // создание класса абстрактной модели друзей
    Q_OBJECT
public:
    enum DataRoles { // перечисление и объявление ролей для связи qml и c++
        loginRole,
        passRole
    };

    stringModel(QObject *parent = 0);

    void addString(const stringOb & newString); // функция добавления друга в список друзей (в модель)
    int rowCount(const QModelIndex & parent = QModelIndex()) const; // функция для подсчета количества друзей в списке
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const; // функция для возвращения данных из переменных объекта друга в qml
protected:
    QHash<int, QByteArray> roleNames() const; // описание ролей
private:
    QList<stringOb> ob_strings; // список объектов друзей

};





class cryptofile : public QObject
{
    Q_OBJECT
public:
    explicit cryptofile(QObject *parent = nullptr);
    QString msg;
    stringModel mod;
signals:

public slots:
    void enc(QString mesg);
    void encryptFile(QByteArray message, char * key, char * iv); // функция шифрования
    void DencryptFile(char * key, char * iv); // функция дешифрования
};

#endif // CRYPTOFILE_H
