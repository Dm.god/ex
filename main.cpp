#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "cryptofile.h"
#include <QByteArray>
#include <QQmlContext>
#include <QObject>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    cryptofile encryption;
    QByteArray msg1("login password1");
    encryption.encryptFile(msg1, "1234567890", "987654321");
    encryption.DencryptFile("1234567890", "987654321");
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;


    QQmlContext * ctxt = engine.rootContext();

    ctxt->setContextProperty("modelString", &(encryption.mod));


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
QObject * appWindow = engine.rootObjects().first();

    QObject::connect(appWindow,
                     SIGNAL(save(QString)), // чей и какой сигнал
                     &encryption,
                     SLOT(enc(QString)));
  return app.exec();
}
