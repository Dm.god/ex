import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.8
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Tabs")
    signal save(string login)
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page {
            id: vkFriends
            Rectangle {
                id: backgroundfriends
                anchors.fill: parent
                color: "black"
            }



            Item {
                id: item1
                anchors.fill: parent
                   ListView {
                       anchors.fill: parent
                       model: modelString
                       spacing: 20
                       delegate: Rectangle {
                               id: rec1
                               color: "white"
                               height: 160
                               width: parent.width
                               radius: 10
                               anchors.margins: 20
                               opacity: 0.8

                               GridLayout {
                                   anchors.fill: parent
                                   opacity: 1

                                   TextField {
                                       id: id1
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 1
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: login
                                   }
                                   TextField {
                                       id: id2
                                       font.pointSize: 12
                                       color: "black"
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 2
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       text: pass
                                   }
                                   Button {
                                       id: id3
                                       font.pointSize: 12
                                       anchors.left: parent.mid
                                       anchors.leftMargin: 10
                                       Layout.row: 3
                                       Layout.column: 1
                                       anchors.horizontalCenter: parent.horizontalCenter
                                       onClicked: save(id1.text + " " + id2.text)
                                       text: "Сохранить"
                                   }


                               }
                           }
                   }
            }
        }

        Page {

        }
    }
}
